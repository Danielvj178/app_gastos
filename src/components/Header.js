import React from 'react';
import './Header.css';
export default function Header(props) {
	const { titulo, year } = props;
	return (
		<header>
			<div className="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm">
				<img
					className="mr-3"
					src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQT5ceJ7hkT0bHT8Y5vi2NBfTJmowpfZeCZJ-vaAUn9x_Ydk2XAlg"
					alt={titulo}
					width="48"
					height="48"
				/>
				<div className="lh-100">
					<h6 className="mb-0 text-white lh-100">{titulo}</h6>
					<small>Since {year}</small>
				</div>
			</div>
		</header>
	);
}
