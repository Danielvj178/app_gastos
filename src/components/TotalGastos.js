import React from 'react';
import { NumberFormat } from '../Helper';

export default function TotalGastos({ totalGastos }) {
	return (
		<li className="list-group-item d-flex justify-content-between bg-purple text-white lh-100">
			<span>Total (USD)</span>
			<strong>${NumberFormat(totalGastos)}</strong>
		</li>
	);
}
