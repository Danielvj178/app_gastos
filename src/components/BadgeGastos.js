import React from 'react';

export default function BadgeGastos({ cantidadGastos }) {
	return (
		<h4 className="d-flex justify-content-between align-items-center mb-3">
			<span className="text-muted">Cantidad Gastos</span>
			<span className="badge badge-secondary badge-pill bg-purple text-white lh-100">{cantidadGastos}</span>
		</h4>
	);
}
